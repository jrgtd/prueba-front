import '@testing-library/jest-dom';
import { render as rtlRender } from '@testing-library/react';
import { cartEndpoint } from 'modules/common/requests/addProduct';
import { productEndpoint } from 'modules/common/requests/getProduct';
import { productsEndpoint } from 'modules/common/requests/getProducts';
import { rest } from 'msw';
import { setupServer } from 'msw/node';
import { Provider } from 'react-redux';
import { BrowserRouter } from 'react-router-dom';
import { applyMiddleware, createStore } from 'redux';
import thunk from 'redux-thunk';
import rootReducer from 'redux/rootReducer';
import { fakeProduct } from '__mocks__/fakeProduct';
import { fakeProducts } from '__mocks__/fakeProducts';

const createTestStore = () => {
  const store = createStore(rootReducer, applyMiddleware(thunk));
  return store;
};
export const render = (component) => {
  const store = createTestStore();
  const render = rtlRender(
    <Provider store={store}>
      <BrowserRouter>{component}</BrowserRouter>
    </Provider>
  );
  return { store, render };
};

export const server = setupServer(
  rest.get(productsEndpoint, async (req, res, ctx) => {
    return res(ctx.json(fakeProducts));
  }),
  rest.get(`${productEndpoint}/${fakeProduct.id}`, async (req, res, ctx) => {
    return res(ctx.json(fakeProduct));
  }),
  rest.post(cartEndpoint, async (req, res, ctx) => {
    return res(ctx.json({ count: 1 }));
  })
);

beforeAll(() => server.listen());
afterAll(() => server.close());
afterEach(() => server.resetHandlers());
