import { combineReducers } from 'redux';
import productsReducer from 'redux/products/products.reducer';
import cartReducer from 'redux/cart/cart.reducer';
import themeReducer from 'redux/theme/theme.reducer';
import productReducer from 'redux/product/product.reducer';

const rootReducer = combineReducers({
  products: productsReducer,
  cart: cartReducer,
  theme: themeReducer,
  product: productReducer
});

export default rootReducer;
