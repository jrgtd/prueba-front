import { productActionsTypes } from 'redux/product/product.actionTypes';

const productInitialState = {
  productId: null,
  status: 'idle',
  error: null
};

const productReducer = (state = productInitialState, action) => {
  switch (action.type) {
    case productActionsTypes.changeProductId:
      return { ...state, productId: action.payload };
    case productActionsTypes.fetchStarted:
      return { ...state, status: 'loading' };
    case productActionsTypes.fetchSuccess:
      return { ...state, status: 'succeeded', product: action.payload };
    case productActionsTypes.fetchFailed:
      return { ...state, status: 'failed', error: action.payload };
    default:
      return state;
  }
};
export default productReducer;
