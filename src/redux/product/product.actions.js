import { getProduct } from 'modules/common';
import { productActionsTypes } from 'redux/product/product.actionTypes';

export const changeIdProductAction = (id) => ({
  type: productActionsTypes.changeProductId,
  payload: id
});

export const fetchProductAction = (productId) => {
  return (dispatch) => {
    dispatch(fetchProductsStarted());

    getProduct(productId)
      .then((res) => {
        dispatch(fetchProductsSuccess(res.data));
      })
      .catch((err) => {
        dispatch(fetchProductsFailure(err.message));
      });
  };
};

const fetchProductsStarted = () => ({
  type: productActionsTypes.fetchStarted
});

const fetchProductsSuccess = (products) => ({
  type: productActionsTypes.fetchSuccess,
  payload: products
});

const fetchProductsFailure = (error) => ({
  type: productActionsTypes.fetchFailed,
  payload: error
});
