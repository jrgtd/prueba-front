export const productActionsTypes = {
  fetchStarted: 'FETCH_PRODUCT_STARTED',
  fetchSuccess: 'FETCH_PRODUCT_SUCCESS',
  fetchFailed: 'FETCH_PRODUCT_FAILED',
  changeProductId: 'CHANGE_ITEM_ID'
};
