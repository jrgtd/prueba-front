export { changeIdProductAction, fetchProductAction } from './product.actions';
export { productActionsTypes } from './product.actionTypes';
export { default as product } from './product.reducer';
export {
  selectProductId,
  selectProduct,
  selectProductStatus,
  selectProductError
} from './product.selectors';
