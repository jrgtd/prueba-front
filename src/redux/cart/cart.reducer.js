import { cartActionsTypes } from 'redux/cart/cart.actionTypes';

const cartInitialState = {};

const cartReducer = (state = cartInitialState, action) => {
  switch (action.type) {
    case cartActionsTypes.addProduct:
      return handleProductAddition(state, action.payload);
    case cartActionsTypes.deleteProduct:
      return handleProductSubstraction(state, action.payload);
    case cartActionsTypes.clear:
      return cartInitialState;
    default:
      return state;
  }
};

const handleProductAddition = (state, payload) => {
  if (!state[payload.id]) {
    return { ...state, [payload.id]: { ...payload, count: 1 } };
  }
  const count = state[payload.id].count + 1;
  return { ...state, [payload.id]: { ...payload, count } };
};

const handleProductSubstraction = (state, payload) => {
  if (!state[payload]) {
    return state;
  }
  const newState = { ...state };
  delete newState[payload];
  return newState;
};

export default cartReducer;
