import { cartActionsTypes } from 'redux/cart/cart.actionTypes';

export const addProductToCartAction = (product) => ({
  type: cartActionsTypes.addProduct,
  payload: product
});
export const deleteProductFromCartAction = (productId) => ({
  type: cartActionsTypes.deleteProduct,
  payload: productId
});
export const clearCartAction = () => ({
  type: cartActionsTypes.clear
});
