export const cartActionsTypes = {
  addProduct: 'ADD_PRODUCT_TO_CART',
  deleteProduct: 'DELETE_PRODUCT_FROM_CART',
  clear: 'CLEAR_CART'
};
