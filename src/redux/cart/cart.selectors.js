import { createSelector } from 'reselect';

export const selectCart = (state) => state.cart;
export const selectCartItems = createSelector([selectCart], (cart) => {
  return Object.values(cart).reduce((total, product) => {
    return total + product.count;
  }, 0);
});
