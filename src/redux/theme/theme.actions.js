import { themeActionsTypes } from 'redux/theme/theme.actionTypes';

export const changeThemeAction = () => ({
  type: themeActionsTypes.changeTheme
});
