import { themeActionsTypes } from 'redux/theme/theme.actionTypes';

const themeInitialState = {
  mode: 'light'
};

const themeReducer = (state = themeInitialState, action) => {
  switch (action.type) {
    case themeActionsTypes.changeTheme:
      return {
        ...state,
        mode: state.mode === 'light' ? 'dark' : 'light'
      };
    default:
      return state;
  }
};
export default themeReducer;
