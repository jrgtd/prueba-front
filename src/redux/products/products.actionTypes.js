export const productsActionsTypes = {
  fetchStarted: 'FETCH_PRODUCTS_STARTED',
  fetchSuccess: 'FETCH_PRODUCTS_SUCCESS',
  fetchFailed: 'FETCH_PRODUCTS_FAILED',
  setSearchField: 'SET_SEARCH_FIELD'
};
