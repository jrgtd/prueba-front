export { fetchProductsAction, setSearchFieldAction } from './products.actions';
export { productsActionsTypes } from './products.actionTypes';
export { default as products } from './products.reducer';
export {
  selectProducts,
  selectStatus,
  selectError,
  selectSearchValue,
  selectProductsToShow
} from './products.selectors';
