import { createSelector } from 'reselect';

export const selectProducts = (state) => state.products.products;
export const selectStatus = (state) => state.products.status;
export const selectError = (state) => state.products.error;
export const selectSearchValue = (state) => state.products.searchValue;
export const selectProductsToShow = createSelector(
  [selectProducts, selectSearchValue],
  (products, searchValue) => {
    if (!searchValue) {
      return products;
    }
    const regExp = new RegExp(searchValue, 'gi');
    return products.filter(
      (product) => regExp.test(product.model) || regExp.test(product.brand)
    );
  }
);
