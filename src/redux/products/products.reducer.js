import { productsActionsTypes } from 'redux/products/products.actionTypes';

const productsInitialState = {
  products: [],
  status: 'idle',
  error: null,
  searchValue: ''
};

const productsReducer = (state = productsInitialState, action) => {
  switch (action.type) {
    case productsActionsTypes.fetchStarted:
      return { ...state, status: 'loading' };
    case productsActionsTypes.fetchSuccess:
      return { ...state, status: 'succeeded', products: action.payload };
    case productsActionsTypes.fetchFailed:
      return { ...state, status: 'failed', error: action.payload };
    case productsActionsTypes.setSearchField:
      return { ...state, searchValue: action.payload };
    default:
      return state;
  }
};
export default productsReducer;
