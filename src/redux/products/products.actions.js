import { getProducts } from 'modules/common';
import { productsActionsTypes } from 'redux/products/products.actionTypes';

export const fetchProductsAction = () => {
  return (dispatch) => {
    dispatch(fetchProductsStarted());

    getProducts()
      .then((res) => {
        dispatch(fetchProductsSuccess(res.data));
      })
      .catch((err) => {
        dispatch(fetchProductsFailure(err.message));
      });
  };
};

const fetchProductsStarted = () => ({
  type: productsActionsTypes.fetchStarted
});

const fetchProductsSuccess = (products) => ({
  type: productsActionsTypes.fetchSuccess,
  payload: products
});

const fetchProductsFailure = (error) => ({
  type: productsActionsTypes.fetchFailed,
  payload: error
});

export const setSearchFieldAction = (text) => ({
  type: productsActionsTypes.setSearchField,
  payload: text
});
