import {
  fireEvent,
  screen,
  waitForElementToBeRemoved
} from '@testing-library/react';
import { Product } from 'modules/Product';
import Router from 'react-router-dom';
import { render } from 'setupTests';
import { fakeProduct } from '__mocks__/fakeProduct';

jest.mock('react-router-dom', () => ({
  ...jest.requireActual('react-router-dom'),
  useParams: jest.fn()
}));

beforeEach(() => {
  jest
    .spyOn(Router, 'useParams')
    .mockReturnValue({ productId: fakeProduct.id });
});

describe('Product', () => {
  test('Render login', () => {
    render(<Product />);
    expect(screen.getByText(/Cargando/i)).toBeInTheDocument();
  });

  test('Render product', async () => {
    render(<Product />);
    await waitForElementToBeRemoved(() => screen.getByText('Cargando'));
    expect(
      screen.getByText(`Modelo: ${fakeProduct.model}`)
    ).toBeInTheDocument();
  });

  test('Add product to cart', async () => {
    const { store } = render(<Product />);
    await waitForElementToBeRemoved(() => screen.getByText('Cargando'));
    const button = screen.getByText(/Añadir al carro/i);
    fireEvent.click(button);
    await waitForElementToBeRemoved(() =>
      screen.getByText('Añadiendo al carro')
    );
    const { cart } = store.getState();
    expect(cart).toHaveProperty(fakeProduct.id);
  });
});
