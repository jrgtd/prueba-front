import { screen, waitForElementToBeRemoved } from '@testing-library/react';
import { productsEndpoint } from 'modules/common/requests/getProducts';
import Home from 'modules/Home';
import { rest } from 'msw';
import { render, server } from 'setupTests';
import { fakeProducts } from '__mocks__/fakeProducts';
import userEvent from '@testing-library/user-event';

const selectedProduct = fakeProducts[0];

describe('Home', () => {
  test('Render login', () => {
    render(<Home />);
    expect(screen.getByText(/Cargando/i)).toBeInTheDocument();
  });

  test('Render products', async () => {
    render(<Home />);

    await waitForElementToBeRemoved(() => screen.getByText('Cargando'));
    expect(screen.getByText(selectedProduct.model)).toBeInTheDocument();
  });

  test('Render error', async () => {
    server.use(
      rest.get(productsEndpoint, (req, res, ctx) => {
        return res(ctx.status(500), {
          message: 'An Unexpected Error Occurred',
          code: 0
        });
      })
    );
    render(<Home />);
    await waitForElementToBeRemoved(() => screen.getByText('Cargando'));
    expect(screen.getByText(/error/i)).toBeInTheDocument();
  });

  test('Display products after search', async () => {
    render(<Home />);
    await waitForElementToBeRemoved(() => screen.getByText('Cargando'));
    const search = screen.getByTestId('search-field');
    userEvent.type(search, selectedProduct.model);
    expect(screen.getByText(selectedProduct.model)).toBeInTheDocument();
  });

  test('Not display products after failed search', async () => {
    render(<Home />);
    await waitForElementToBeRemoved(() => screen.getByText('Cargando'));
    const search = screen.getByTestId('search-field');
    userEvent.type(search, 'Test');
    expect(
      screen.getByText('No se han encontrado productos')
    ).toBeInTheDocument();
  });
});
