import { screen } from '@testing-library/react';
import { OptionSelect } from 'modules/common';
import { render } from 'setupTests';
const options = {
  colors: [
    {
      code: 1000,
      name: 'Black'
    }
  ],
  storages: [
    {
      code: 2000,
      name: '512 MB'
    },
    {
      code: 2001,
      name: '256 MB'
    }
  ]
};
describe('Option select', () => {
  test('Selects first option when available', () => {
    render(<OptionSelect options={options.colors} setValue={() => {}} />);
    expect(screen.getByText(/Black/i)).toBeInTheDocument();
  });
  test('Nothing selected when there is more than one option', () => {
    render(<OptionSelect options={options.storages} setValue={() => {}} />);
    expect(screen.queryByText('512 MB')).not.toBeInTheDocument();
  });
});
