import axios from 'axios';
export const productEndpoint =
  'https://front-test-api.herokuapp.com/api/product';

export const getProduct = async (productId) =>
  axios.get(`${productEndpoint}/${productId}`);
