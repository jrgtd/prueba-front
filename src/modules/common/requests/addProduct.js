import axios from 'axios';
export const cartEndpoint = 'https://front-test-api.herokuapp.com/api/cart';

export const addProduct = async (id, colorCode, storageCode) =>
  axios.post(cartEndpoint, {
    id,
    colorCode,
    storageCode
  });
