import axios from 'axios';

export const productsEndpoint =
  'https://front-test-api.herokuapp.com/api/product';

export const getProducts = async () => axios.get(productsEndpoint);
