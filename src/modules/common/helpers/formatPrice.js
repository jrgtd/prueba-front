export const formatPrice = (price) => (price ? `${price}€` : null);
