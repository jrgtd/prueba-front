import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { setSearchFieldAction } from 'redux/products/products.actions';
import { TextField } from 'modules/common';

const Search = ({ setSerchValue }) => {
  const handleOnChange = (event) => {
    setSerchValue(event.target.value);
  };

  return (
    <div style={{ alignSelf: 'flex-end' }}>
      <TextField
        inputProps={{ 'data-testid': 'search-field' }}
        onChange={(event) => {
          handleOnChange(event);
        }}
      />
    </div>
  );
};

Search.propTypes = {
  setSerchValue: PropTypes.func.isRequired
};

const mapDispatchToProps = {
  setSerchValue: setSearchFieldAction
};

export default connect(null, mapDispatchToProps)(Search);
