import React from 'react';
import { Link as RouterLink } from 'react-router-dom';
import styled from 'styled-components';

const StyledLink = styled(RouterLink)`
  text-decoration: none;
`;

export const Link = (props) => {
  return <StyledLink {...props} />;
};
