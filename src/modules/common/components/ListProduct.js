import React from 'react';
import PropTypes from 'prop-types';
import { Typography, Card, CardContent, Link } from 'modules/common';
import styled from 'styled-components';
import { formatPrice } from 'modules/common/helpers';

const TextContainer = styled.div`
  display: flex;
  flex-direction: column;
  gap: 0.5rem;
`;

const SubTextContainer = styled.div`
  display: flex;
  width: 100;
  justify-content: space-between;
`;

const ImageWrapper = styled.div`
  text-align: center;
`;

const Image = styled.img`
  display: inline;
  height: 200px;
  background-size: cover;
  background-repeat: no-repeat;
  background-position: center;
  object-fit: scale-down;
  padding: 10px;
`;

export const ListProduct = ({
  product: { id, brand, model, price, imgUrl }
}) => {
  return (
    <Link to={{ pathname: `/${id}` }}>
      <Card>
        <ImageWrapper>
          <Image src={imgUrl} alt={model} />
        </ImageWrapper>
        <CardContent>
          <TextContainer>
            <Typography variant="body1" align="center">
              {model}
            </Typography>
            <SubTextContainer>
              <Typography variant="body2">{brand}</Typography>
              <Typography variant="body2">{formatPrice(price)}</Typography>
            </SubTextContainer>
          </TextContainer>
        </CardContent>
      </Card>
    </Link>
  );
};

ListProduct.propTypes = {
  product: PropTypes.shape({
    id: PropTypes.string.isRequired,
    brand: PropTypes.string.isRequired,
    model: PropTypes.string.isRequired,
    price: PropTypes.string.isRequired,
    imgUrl: PropTypes.string.isRequired
  })
};
