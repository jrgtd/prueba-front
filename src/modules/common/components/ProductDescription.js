import PropTypes from 'prop-types';
import React from 'react';
import { connect } from 'react-redux';
import { addProductToCartAction } from 'redux/cart/cart.actions';
import styled from 'styled-components';
import {
  Typography,
  Button,
  OptionSelect,
  addProduct,
  CircularProgress
} from 'modules/common';
import { formatPrice } from 'modules/common/helpers';

const ProductContainer = styled.div`
  display: grid;
  padding: 5%;
  height: 100vh;
  padding-top: 100px;
  gap: 1rem;
  grid-template-columns: 1fr;
  grid-template-rows: repeat(2, 1fr);

  @media (min-width: 768px) {
    padding-top: 5%;
    grid-template-columns: repeat(2, 1fr);
    grid-template-rows: 1fr;
  }
  justify-content: center;
  align-items: center;
`;

const Image = styled.img`
  width: auto;
  @media (min-width: 768px) {
    width: 50%;
  }
`;

const GenericContainer = styled.div`
  display: flex;
  width: 100%;
  justify-content: center;
  align-items: center;
`;

const InfoContainer = styled(GenericContainer)`
  flex-direction: column;
  gap: 1rem;
`;

const ImageContainer = styled(GenericContainer)``;

const TextContainer = styled(GenericContainer)`
  flex-direction: column;
  gap: 0.5rem;
`;

const ActionsContainer = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  width: 100%;
  gap: 0.5rem;
  max-width: 400px;
`;

const ProductDescription = ({ addProductToCart, product }) => {
  const [loading, setLoading] = React.useState(false);
  const [colorCode, setColorCode] = React.useState(null);
  const [storageCode, setStorageCode] = React.useState(null);
  const {
    id,
    brand,
    model,
    price,
    imgUrl,
    options: { colors, storages }
  } = product;
  return (
    <ProductContainer>
      <ImageContainer>
        <Image src={imgUrl} alt={model} />
      </ImageContainer>
      <InfoContainer>
        <TextContainer>
          <Typography variant="body1">Modelo: {model}</Typography>
          <Typography variant="body1">Marca: {brand}</Typography>
          <Typography variant="body1">
            Precio: {price ? formatPrice(price) : 'No disponible'}
          </Typography>
        </TextContainer>
        <ActionsContainer>
          <OptionSelect setValue={setColorCode} options={colors} />
          <OptionSelect setValue={setStorageCode} options={storages} />
          <Button
            disabled={!colorCode || !storageCode || loading}
            onClick={() => {
              setLoading(true);
              addProduct(id, colorCode, storageCode).then(() => {
                setLoading(false);
                addProductToCart(product);
              });
            }}
          >
            Añadir al carro
          </Button>
          {loading ? (
            <>
              <Typography variant="body2">Añadiendo al carro</Typography>
              <CircularProgress />
            </>
          ) : null}
        </ActionsContainer>
      </InfoContainer>
    </ProductContainer>
  );
};

ProductDescription.propTypes = {
  product: PropTypes.shape({
    id: PropTypes.string.isRequired,
    brand: PropTypes.string.isRequired,
    model: PropTypes.string.isRequired,
    price: PropTypes.string.isRequired,
    imgUrl: PropTypes.string.isRequired,
    options: PropTypes.shape({
      colors: PropTypes.array,
      storages: PropTypes.array
    })
  }),
  addProductToCart: PropTypes.func.isRequired
};

const mapDispatchToProps = {
  addProductToCart: addProductToCartAction
};

export default connect(null, mapDispatchToProps)(ProductDescription);
