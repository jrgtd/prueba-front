import { FullScreenContainer, Typography } from 'modules/common';
import PropTypes from 'prop-types';
import React from 'react';

export const ErrorScreen = ({ text }) => {
  return (
    <FullScreenContainer>
      <Typography variant="h5">{text}</Typography>
    </FullScreenContainer>
  );
};
ErrorScreen.propTypes = {
  text: PropTypes.string.isRequired
};
