import React from 'react';
import {
  Typography,
  CircularProgress,
  FullScreenContainer
} from 'modules/common';

export const Loading = () => {
  return (
    <FullScreenContainer>
      <>
        <CircularProgress />
        <Typography variant="h5">Cargando</Typography>
      </>
    </FullScreenContainer>
  );
};
