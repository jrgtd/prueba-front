import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { Button, Icon, Typography, Drawer } from 'modules/common';
import { selectCart } from 'redux/cart/cart.selectors';
import { connect } from 'react-redux';
import {
  clearCartAction,
  deleteProductFromCartAction
} from 'redux/cart/cart.actions';
import { Close } from '@mui/icons-material';

const CardWrapper = styled.div`
  padding: 10%;
  width: 90vw;
  display: flex;
  flex-direction: column;
  gap: 1rem;
  position: relative;
  @media (min-width: 768px) {
    width: 40vw;
  }
  @media (min-width: 1200px) {
    width: 30vw;
  }
`;

const ProductWrapper = styled.div`
  display: flex;
  flex-direction: column;
  border: 2px solid grey;
  padding: 10px;
  gap: 0.5rem;
`;

const CloseWrapper = styled.div`
  position: absolute;
  top: 20px;
  right: 20px;
`;

const cartIsEmpty = (cart) => Object.keys(cart).length === 0;

const CartLayer = ({
  cart,
  deleteProduct,
  clearCart,
  setCartIsOpen,
  cartIsOpen
}) => {
  return (
    <Drawer open={cartIsOpen}>
      <CardWrapper>
        <Typography variant="h5">Carro de la compra</Typography>
        <CloseWrapper
          onClick={() => {
            setCartIsOpen(false);
          }}
        >
          <Icon>
            <Close />
          </Icon>
        </CloseWrapper>
        {cartIsEmpty(cart) ? (
          <Typography variant="h6">No hay productos en el carro</Typography>
        ) : (
          Object.keys(cart).map((id) => (
            <ProductWrapper key={id}>
              <Typography>Modelo: {cart[id].model}</Typography>
              <Typography>Cantidad: {cart[id].count}</Typography>
              <Button
                onClick={() => {
                  deleteProduct(id);
                }}
              >
                Eliminar
              </Button>
            </ProductWrapper>
          ))
        )}
        <Button
          disabled={cartIsEmpty(cart)}
          onClick={() => {
            clearCart();
          }}
        >
          Vaciar carro
        </Button>
      </CardWrapper>
    </Drawer>
  );
};
CartLayer.propTypes = {
  cart: PropTypes.object.isRequired,
  cartIsOpen: PropTypes.bool.isRequired,
  deleteProduct: PropTypes.func.isRequired,
  clearCart: PropTypes.func.isRequired,
  setCartIsOpen: PropTypes.func.isRequired
};

const mapStateToProps = (state) => ({
  cart: selectCart(state)
});

const mapDispatchToProps = {
  deleteProduct: deleteProductFromCartAction,
  clearCart: clearCartAction
};

export default connect(mapStateToProps, mapDispatchToProps)(CartLayer);
