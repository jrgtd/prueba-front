import LightModeIcon from '@mui/icons-material/LightMode';
import ShoppingCartIcon from '@mui/icons-material/ShoppingCart';
import PropTypes from 'prop-types';
import React from 'react';
import { connect } from 'react-redux';
import { changeThemeAction } from 'redux/theme';
import styled from 'styled-components';
import {
  CartLayer,
  Icon,
  Link,
  Typography,
  AppBar,
  Badge,
  Breadcrumbs
} from 'modules/common';
import { selectCartItems } from 'redux/cart';
import { useLocation } from 'react-router-dom';
import { selectProduct } from 'redux/product';

const HeaderWrapper = styled.div`
  padding: 8px 16px;
  display: flex;
  justify-content: space-between;
  align-items: center;
`;

const IconsWrapper = styled.div`
  display: flex;
  gap: 12px;
  position: relative;
`;

const Header = ({ changeTheme, cartItems, product }) => {
  return (
    <AppBar>
      <HeaderWrapper>
        <Link to={{ pathname: '/' }}>
          <Typography color="inherit" variant="h6">
            Logo
          </Typography>
        </Link>
        <Breadcrumb product={product} />
        <HeaderIcons changeTheme={changeTheme} cartItems={cartItems} />
      </HeaderWrapper>
    </AppBar>
  );
};

const Breadcrumb = ({ product }) => {
  const [showProductBreadcrumb, setShowProductBreadcrumb] =
    React.useState(false);
  const location = useLocation();
  React.useEffect(() => {
    if (product && location.pathname.includes(product.id)) {
      setShowProductBreadcrumb(true);
    } else {
      setShowProductBreadcrumb(false);
    }
  }, [location, product]);

  return (
    <Breadcrumbs>
      <Link to={{ pathname: '/' }}>Home</Link>
      {showProductBreadcrumb ? (
        <Link to={{ pathname: `/${product.id}` }}>{product.model}</Link>
      ) : null}
    </Breadcrumbs>
  );
};

const HeaderIcons = ({ changeTheme, cartItems }) => {
  const [cartIsOpen, setCartIsOpen] = React.useState(false);

  return (
    <IconsWrapper>
      <CartLayer cartIsOpen={cartIsOpen} setCartIsOpen={setCartIsOpen} />
      <Badge badgeContent={cartItems} color="secondary">
        <Icon>
          <ShoppingCartIcon
            onClick={() => {
              setCartIsOpen(true);
            }}
          />
        </Icon>
      </Badge>
      <Icon>
        <LightModeIcon
          onClick={() => {
            changeTheme();
          }}
        />
      </Icon>
    </IconsWrapper>
  );
};

Header.propTypes = {
  product: PropTypes.object.isRequired,
  cartItems: PropTypes.number.isRequired,
  changeTheme: PropTypes.func.isRequired
};

HeaderIcons.propTypes = {
  cartItems: PropTypes.number.isRequired,
  changeTheme: PropTypes.func.isRequired
};

Breadcrumb.propTypes = { product: PropTypes.object.isRequired };

const mapStateToProps = (state) => ({
  cartItems: selectCartItems(state),
  product: selectProduct(state)
});

const mapDispatchToProps = {
  changeTheme: changeThemeAction
};

export default connect(mapStateToProps, mapDispatchToProps)(Header);
