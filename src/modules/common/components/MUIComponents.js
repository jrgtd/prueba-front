import { TextField as MUITextField } from '@mui/material';
import { Icon as MUIIcon } from '@mui/material';
import styled from 'styled-components';
import { Typography as MUITypography } from '@mui/material';
import { Button as MUIButton } from '@mui/material';
import { AppBar as MUIAppBar } from '@mui/material';
import { Drawer as MUIDrawer } from '@mui/material';
import { Card as MUICard } from '@mui/material';
import { CardContent as MUICardContent } from '@mui/material';
import { CircularProgress as MUICircularProgress } from '@mui/material';
import { MenuItem as MUIMenuItem } from '@mui/material';
import { Select as MUISelect } from '@mui/material';
import { Badge as MUIBadge } from '@mui/material';
import { Breadcrumbs as MUIBreadcrumbs } from '@mui/material';

export const TextField = (props) => {
  return <MUITextField {...props} />;
};

export function Typography(props) {
  return <MUITypography {...props} />;
}

const StyledIcon = styled(MUIIcon)`
  cursor: pointer;
`;

export function Icon(props) {
  return <StyledIcon {...props} />;
}

const StyledButton = styled(MUIButton)`
  max-width: 200px;
`;

export const Button = (props) => {
  return <StyledButton variant="contained" {...props} />;
};

export const AppBar = (props) => {
  return <MUIAppBar color="primary" {...props} />;
};
export const Drawer = (props) => {
  return <MUIDrawer anchor="right" {...props} />;
};
export const Card = (props) => {
  return <MUICard {...props} />;
};
export const CardContent = (props) => {
  return <MUICardContent {...props} />;
};
export const CircularProgress = (props) => {
  return <MUICircularProgress {...props} />;
};
export const MenuItem = (props) => {
  return <MUIMenuItem {...props} />;
};

export const Select = (props) => {
  return <MUISelect {...props} />;
};

export const Badge = (props) => {
  return <MUIBadge {...props} />;
};

export const Breadcrumbs = (props) => {
  return <MUIBreadcrumbs {...props} />;
};
