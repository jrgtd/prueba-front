import React from 'react';
import PropTypes from 'prop-types';
import styled, { createGlobalStyle } from 'styled-components';
import { useTheme } from '@mui/material';
import Header from 'modules/common/components/Header';

const GlobalStyle = createGlobalStyle`
  * {
    box-sizing: border-box;
    margin: 0;
    padding: 0;
    color: ${(props) => props.theme.palette.text.primary};
  }

`;

const Container = styled.div`
  display: flex;
  flex-direction: column;
  width: 100vw;
  height: 100%;
  min-height: 100vh;
  grid-auto-rows: 50px 1fr;
  background-color: ${(props) => props.theme.palette.background.default};
`;

export function Layout(props) {
  const theme = useTheme();
  return (
    <>
      <GlobalStyle theme={theme} />
      <Container theme={theme}>
        <Header />
        {props.children}
      </Container>
    </>
  );
}

Layout.propTypes = {
  children: PropTypes.element
};
