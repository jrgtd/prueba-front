import React from 'react';
import PropTypes from 'prop-types';
import { MenuItem, Select } from 'modules/common';

export const OptionSelect = ({ options, setValue }) => {
  React.useEffect(() => {
    if (options.length === 1) setValue(options[0].code);
  }, []);

  return (
    <Select defaultValue={options.length === 1 ? options[0].code : ''}>
      {options.map((option) => (
        <MenuItem
          key={option.code}
          value={option.code}
          onClick={() => {
            setValue(option.code);
          }}
        >
          {option.name}
        </MenuItem>
      ))}
    </Select>
  );
};

OptionSelect.propTypes = {
  options: PropTypes.array.isRequired,
  setValue: PropTypes.func.isRequired
};
