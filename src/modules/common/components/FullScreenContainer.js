import styled from 'styled-components';
import PropTypes from 'prop-types';

const Container = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  width: 100%;
  height: 100vh;
  gap: 1rem;
`;
export const FullScreenContainer = ({ children }) => (
  <Container>{children}</Container>
);

FullScreenContainer.propTypes = {
  children: PropTypes.element
};
