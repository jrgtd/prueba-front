import { Loading, ProductDescription } from 'modules/common';
import PropTypes from 'prop-types';
import React from 'react';
import { connect } from 'react-redux';
import { useParams } from 'react-router-dom';
import { addProductToCartAction } from 'redux/cart/cart.actions';
import { fetchProductAction } from 'redux/product/product.actions';
import { changeIdProductAction } from 'redux/product/product.actions';
import {
  selectProductError,
  selectProduct,
  selectProductStatus
} from 'redux/product/product.selectors';
import { Navigate } from 'react-router-dom';

const Product = ({ changeProductId, product, fetchProduct, status }) => {
  let params = useParams();

  React.useEffect(() => {
    const productId = params.productId;
    changeProductId(productId);
    fetchProduct(productId);
  }, [params]);

  return status === 'loading' || status === 'idle' ? (
    <Loading />
  ) : status === 'succeeded' && product ? (
    <ProductDescription product={product} />
  ) : (
    <Navigate to={{ pathname: '/' }} />
  );
};

Product.propTypes = {
  product: PropTypes.object,
  changeProductId: PropTypes.func.isRequired,
  fetchProduct: PropTypes.func.isRequired,
  addProductToCart: PropTypes.func.isRequired,
  status: PropTypes.string.isRequired,
  error: PropTypes.string
};

const mapStateToProps = (state) => ({
  product: selectProduct(state),
  status: selectProductStatus(state),
  error: selectProductError(state)
});

const mapDispatchToProps = {
  changeProductId: changeIdProductAction,
  fetchProduct: fetchProductAction,
  addProductToCart: addProductToCartAction
};

export default connect(mapStateToProps, mapDispatchToProps)(Product);
