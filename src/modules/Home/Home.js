import {
  ListProduct,
  Loading,
  Search,
  Typography,
  ErrorScreen
} from 'modules/common';
import PropTypes from 'prop-types';
import React from 'react';
import { connect } from 'react-redux';
import { fetchProductsAction } from 'redux/products/products.actions';
import {
  selectProducts,
  selectProductsToShow,
  selectStatus
} from 'redux/products/products.selectors';
import styled from 'styled-components';

const Grid = styled.div`
  display: grid;
  grid-template-columns: 1fr;

  @media (min-width: 425px) {
    grid-template-columns: repeat(2, 1fr);
  }
  @media (min-width: 768px) {
    grid-template-columns: repeat(3, 1fr);
  }
  @media (min-width: 1200px) {
    grid-template-columns: repeat(4, 1fr);
  }
  grid-column-gap: 1rem;
  grid-row-gap: 5rem;
`;

const HomeLayout = styled.div`
  padding: 20px 5%;
  display: flex;
  flex-direction: column;
  gap: 20px;
  padding-top: 100px;
`;

const Home = ({ products, fetchProducts, productsToShow, status }) => {
  React.useEffect(() => {
    if (products.length !== 0) return;
    fetchProducts();
  }, []);

  return status === 'loading' || status === 'idle' ? (
    <Loading />
  ) : status === 'succeeded' && productsToShow ? (
    <HomeLayout>
      <Search />
      <Grid>
        {productsToShow.length === 0 ? (
          <Typography variant="h2">No se han encontrado productos</Typography>
        ) : (
          productsToShow.map((product) => {
            return <ListProduct product={product} key={product.id} />;
          })
        )}
      </Grid>
    </HomeLayout>
  ) : (
    <ErrorScreen text="Ha ocurrido un error" />
  );
};

Home.propTypes = {
  products: PropTypes.array.isRequired,
  productsToShow: PropTypes.array.isRequired,
  fetchProducts: PropTypes.func.isRequired,
  status: PropTypes.string.isRequired
};

const mapStateToProps = (state) => ({
  products: selectProducts(state),
  productsToShow: selectProductsToShow(state),
  status: selectStatus(state)
});

const mapDispatchToProps = {
  fetchProducts: fetchProductsAction
};

export default connect(mapStateToProps, mapDispatchToProps)(Home);
