import { createTheme, ThemeProvider } from '@mui/material';
import { Layout } from 'modules/common';
import Home from 'modules/Home';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import PropTypes from 'prop-types';
import { Product } from 'modules/Product';
import { connect } from 'react-redux';
import { selectTheme } from 'redux/theme';

function App({ mode }) {
  const customTheme = createTheme({
    palette: {
      mode: mode,
      background: {
        default: mode === 'light' ? '#f3f4f6' : '#121212'
      }
    },
    typography: {
      fontFamily: 'Roboto, Arial'
    }
  });
  return (
    <BrowserRouter>
      <ThemeProvider theme={customTheme}>
        <Layout>
          <Routes>
            <Route path="/" element={<Home />} />
            <Route path=":productId" element={<Product />} />
          </Routes>
        </Layout>
      </ThemeProvider>
    </BrowserRouter>
  );
}

App.propTypes = {
  mode: PropTypes.string.isRequired
};

const mapStateToProps = (state) => ({
  mode: selectTheme(state)
});

export default connect(mapStateToProps)(App);
